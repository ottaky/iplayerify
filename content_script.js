setTimeout(() => {
  let anchors = document.getElementsByTagName('a')
  for (let idx = 0; idx < anchors.length; idx++) {
    if (anchors[idx].href.match(/episode\/[^\/]+\//)) {
      let pid = anchors[idx].href.match(/episode\/([^\/]+)\//)[1]
      let new_div = document.createElement('div')
      new_div.innerText = `get_iplayer --get --pid ${pid}`
      new_div.style.color = '#D0D0D0'
      new_div.style.userSelect = 'text'
      new_div.style.paddingBottom = '5px'
      let pn = anchors[idx].parentNode
      pn.style.border = '1px solid #f54997'
      pn.style.padding = '5px'
      pn.insertBefore(new_div, pn.children[0])
    }
  }
}, 1000)

